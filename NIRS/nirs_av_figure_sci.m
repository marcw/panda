% function nirs_av_grandaverage
% determine grand average
% signals are obtained from
clear all
close all
% Load mat files
cd('/Users/marcw/DATA/NIRS/OXY3_v14112014'); %#ok<*UNRCH> % contains all relevant data files
d		= dir('nirs0*.mat');

[m,~]	= size(d);
nmbr	= NaN(m,1);
for ii	= 1:m
	fname		= d(ii).name;
	nmbr(ii)	= str2double(fname(5:7));
	
end
unmbr			= unique(nmbr);
nnmbr			= numel(unmbr);
[~,Txt]	= xlsread('subj_analysis_v14112014-all.xlsx');
Txt				= Txt(2:end,:);

SCI = [];
F = cell(1);
C = [];
cnt = 0;
for ii = 1:nnmbr
	disp('==============')
	sel		= nmbr == unmbr(ii);
	fnames	= {d(sel).name};
	nfiles	= numel(fnames);
	% 	disp(ii)
	disp(fnames)
	S		= [];
	So = [];
	T		= [];
	M		= [];
	E		= [];
	
	for jj = 1:nfiles
		% 		fnames{jj}
		load(fnames{jj})
		sci = nirs.sci;
		SCI = [SCI;sci]; %#ok<*AGROW>
		for kk = 1:numel(sci)
			cnt = cnt+1;
			F{cnt} = fnames{jj};
			C(cnt) = kk;
		end
	end
end

%%
sel = SCI<0.9;
f = F(sel)

[SCI(sel) C(sel)']
