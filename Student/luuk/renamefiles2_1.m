cd /Users/marcw/DATA/Test/SPIN/Results/2_1

d = dir;

sel = [d.isdir];
d = d(~sel);

nfiles = numel(d);
for ii = 1:nfiles
	fname = d(ii).name
	
	newname = fname;
	idx = strfind(fname,'.');
	newname(idx) = '_';
	newname = fcheckext(newname,'mat');
	s = movefile(fname,newname);
	s
end