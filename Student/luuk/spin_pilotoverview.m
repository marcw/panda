cd('/Users/marcw/DATA/Test/SPIN/Results');
d = dir;
dirnames = {d(3:end).name}; % first 2 are . and ..
ndir = numel(dirnames);
T = [];
L = [];
S = [];
C = [];
O = [];
R = [];
SNR = [];
exc = {'koopt','negen','vuile','boeken',''};
order = 1:5;

for dirIdx = 1:ndir
	% 				dirnames{dirIdx};
	cd(dirnames{dirIdx});
	matfiles = dir('*.mat');
	matfiles = {matfiles.name};
	nfiles = numel(matfiles);
	for fIdx = 1:nfiles
		fname = matfiles{fIdx};
		load(fname);
		stim		= rec.wordstimulus;
		if strcmp(stim,exc)
			% 						matfiles{fIdx}
			stim = {'Jan','koopt','negen','vuile','boeken'};
		end
		res			= rec.wordresponse;
		correct		= strcmp(stim,res);
		nstim		= numel(stim);
		
		% bug - speechlevel is not recorded per trial
		% try to find in fname
		tr		= fname;
		idx1	= strfind(tr,'_tr');
		idx2	= strfind(tr,'_');
		if idx2(2)>idx1
			idx2	= idx2(2)-1;
		else
			idx2	= idx2(3)-1;
		end
		idx1	= idx1+3;
		tr		= str2double(tr(idx1:idx2));
		level	= rec.speechLevels(tr);
		
		
		snr			= repmat(level-str2double(rec.settings.noiseLvl),1,nstim);
		list		= repmat(rec.listNr,1,nstim);
		subject		= repmat(str2double(rec.subjectId(1)),1,nstim);
		T			= [T stim];
		R = [R res];
		C			= [C correct];
		SNR			= [SNR snr];
		L			= [L list];
		S			= [S subject];
		O			= [O order];
	end
	cd ..
end

[ut,~,t] = unique(T);

x = SNR';
y = double(C)';
s = S'; % subjects
s = L'; % lists
s = t; % words
O = O';
% 		s = O'; % word order in sentence

%%
Tr = reshape(T,5,length(T)/5);
whos Tr
M = NaN(size(Tr));
for ii = 1:5
	[ut,~,idx] = unique(Tr(ii,:));
	M(ii,:) = idx;
end
close all
plot(M)
axis square;
axis([0 6 0 11]);
set(gca,'TickDir','out');
box off
%%
close all
nt = numel(ut);
M = NaN(nt,nt);
for kk = 1:5
	sel = O==kk;
	t = T(sel);
	r = R(sel);
	ut = unique(t);
	nt = numel(ut);
	M = NaN(nt,nt);
	
	for ii = 1:nt
		for jj = 1:nt
			sel		= strcmp(ut(ii),t) & strcmp(ut(jj),r);
			N		= sum(strcmp(ut(ii),t));
			M(jj,ii) = sum(sel)/N;
		end
	end
	
	figure(1)
	subplot(2,3,kk)
	imagesc(M);
	
	caxis([0 .3])
	colorbar;
	axis square;
	box off
	set(gca,'YDir','normal','TickDir','out','YTick',1:50,'YTickLabel',ut,'XTick',1:50,'XTickLabel',ut,'XTickLabelRotation',90);
	title(['Order: ' num2str(kk)])
	xlabel('Target');
	ylabel('Response');
end
savegraph('wordconfusion','png');