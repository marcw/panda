close all
clear all

				fname = which('ILD_const_high_300.mat');
				load(fname)
				
% plot(x,y,'.')
% 
% xi		= linspace(0.01,max(x),100);
% 
% sel		= y==0;
% f0		= ksdensity(x(sel),xi,'support','positive');
% plot(xi,f0,'o-','MarkerFaceColor','w');
% hold on
% 
% 
% sel		= y==1;
% f1		= ksdensity(x(sel),xi,'support','positive');
% plot(xi,f1,'o-','MarkerFaceColor','w');
% hold on
% 
% f = f1./f0;
% f = f-min(f);
% f = f./max(f);
% plot(xi,f);
% 
% return
guessrate = 0.5;
lapserate = 'infer';
psifit(x,y,s,'gamma',guessrate,'lambda',lapserate,'function',@revweibullfun,'numSavedSteps',20000,'showCentroid','mean','showDiag',true);


xlabel('ILD (dB)');
savegraph('ild','png');				
return
%% ITD
close all
clear all

fname = which('ild02_SA.mat');
load(fname)
x = uml.x;
y = uml.r;

% 		x = [-x; x];
% 		y = [1-y; y];

% add some X=0 Y=random Bernouilli values, to make sure there are no P>0
% for x<0
n = 100;
x = [zeros(n,1);x];
y = [rndval(0,1,[n,1]);y];
s = ones(size(x));

guessrate = 0.5;
lapserate = 0;
psifit(x,y,s,'gamma',guessrate,'lambda',lapserate);
xlabel('ILD (dB)');
savegraph('ild','png');


%% ITD
close all
clear all

fname = which('itd02_SA.mat');
load(fname)
x = uml.x;
y = uml.r;

% 		x = [-x; x];
% 		y = [1-y; y];

% add some X=0 Y=random Bernouilli values, to make sure there are no P>0
% for x<0
n = 100;
x = [zeros(n,1);x];
y = [rndval(0,1,[n,1]);y];
s = ones(size(x));
guessrate = 0.5;
lapserate = 0;
psifit(x,y,s,'gamma',guessrate,'lambda',lapserate);
xlabel('ITD (\mus)');
savegraph('itd','png');